# Krita.org website

Marketing website for the open source art application Krita. Built with the Hugo static site generator. Currently deployed at https://dev.krita.org

## Building and testing the site locally

This site is based on [hugo-kde theme](https://invent.kde.org/websites/hugo-kde/), which is the common theme for various KDE websites that are based on Hugo. Follow [the guide on its wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/Getting-started) to know what are needed to use the theme and how to build a website with Hugo.

If you see a white page, you might have to run this before the "hugo server" command

    hugo mod clean

This usually means there was an update to the hugo theme, and your cached version if messing it up.

## Making changes/additions

See the [USAGE](/USAGE.md) guide for every day updates

# Changing Templates/Layouts

If you go in the themes > krita-org-theme > layouts, you will notice there are a lot of template files there. This is a template that a page can use. If a page(MD file) is using a template, it will have a "type" variable set at the top. This maps directly to the folder in this layout area.

Template variable errors can be very cryptic to read when doing things like looping over content. Do small changes at a time to make it easier to know what went wrong.

Note: If you are running the project and make changes to the template, you might have to stop the server from running (I use Ctrl+C), then restart it again with 'hugo server' to see the changes.

# Adding tabs to groups of pages

Some website sections like the About area has multiple pages inside (History, About, Press, etc ). There are two parts to making this sub-navigation work
1. Adding a "group" variable to the top of each page that needs to be set to the section.
2. Having a template (type variable) that that shows and filters that data. Templates are in the theme > krita-org-theme > layouts folder

# TODO items
1. Since this is a static site, it has no logic with when to display a 404 page. That will have to be configured on the server whenever we start testing it out
- For testing locally, you should be able to go to your /404.html (mine is http://localhost:1313/404.html)

2. Update features icons to svg icons from source code

3. Mollie one-time payment needs to be hooked up. This might also need something extra to get the PHP page to work for the redirect and back-end