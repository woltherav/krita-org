---
title: "Shop"
layout: simple
date: '2018-05-12'
menu:
  main:
    weight: 5
items:
  usbcard:
    imageUrl: images/pages/usbcard.jpg
    title: USB keycard with Krita (latest) and training
    description: USB keycard with the newest stable version of Krita for all OSes. Includes Comics with Krita, Muses, Secrets of Krita, Animate with Krita and Digital Atelier tutorial packs.
    price: €34.95
    buyText: Get USB keycard
    buyUrl: https://gumroad.com/l/qQmZf
  bundle:
    imageUrl: "images/pages/DA_cover_cropped_1_1.png"
    title: "Ramon Miranda's Digital Atelier Bundle"
    description: "All the brushes, brush tips, patterns and surfaces from Digital Atelier."
    price: "€4.95"
    buyText: "Get Digital Atelier Bundle"
    buyUrl: "https://gumroad.com/l/ehZUc"
  bundle2:
    imageUrl: "images/pages/DA_cover_cropped_1_1.png"
    title: "Ramon Miranda's Digital Atelier Bundle (Complete)"
    description: "A complete new brush preset bundle: Digital Atelier. Not only does this contain over fifty new brush presets, more than thirty new brush tips and twenty patterns and surfaces, there is two hours of in-depth tutorial on creating brush presets."
    price: "€19.95"
    buyText: "Get Digital Atelier Bundle"
    buyUrl: "https://gum.co/sFbEb"
  secret:
    imageUrl: "images/pages/secrets-of-krita-box-art.png"
    title: "Training: Secrets of Krita (DVD)"
    description: "Comics with Krita author Timothée Giet is back with his second training DVD: Secrets of Krita. Ten chapters covering topics like brush settings, layer handling, transformations and more! In depth and in detail. The DVD is English spoken with English subtitles. [Read more about it](/posts/secrets-of-krita-the-third-krita-training-dvd/)!"
    price: "€14.95"
    buyText: "Get Secrets of Krita"
    buyUrl: "https://gum.co/iIno?wanted=true"
  muses:
    imageUrl: "images/pages/muses.jpg"
    title: "Training: Muses (Download)"
    description: "Five hours of detail-packed instruction, covering the whole process from setting up Krita to printing a finished work of art. The DVD isn’t just about using Krita’s interface. It’s a complete creative course. The video is Spanish spoken with English and Spanish subtitles."
    price: "€14.95"
    buyText: "Get Muses"
    buyUrl: "https://gum.co/SZZDI?wanted=true"
  secret_dl:
    imageUrl: "images/pages/secrets-of-krita-box-art.png"
    title: "Training: Secrets of Krita (Download)"
    description: "Comics with Krita author Timothée Giet is back with his second training DVD: Secrets of Krita. Ten chapters covering topics like brush settings, layer handling, transformations and more! In depth and in detail. The DVD is English spoken with English subtitles. [Read more](/posts/secrets-of-krita-the-third-krita-training-dvd/)!"
    price: "€14.95"
    buyText: "Get Secrets of Krita"
    buyUrl: "https://gum.co/bDeXV?wanted=true"
  animate:
    imageUrl: "images/pages/coverSize-246x300.png"
    title: "Training: Animate with Krita (Download)"
    description: "Krita's exciting new animation feature explained by Timothee Giet! Krita 3.0 and up comes with tools to create traditional hand-drawn 2D animation. Create the next Looney Tunes with Krita! This is a preview release, containing two of three planned parts. Everyone who buys now will receive the third part when it is ready. The first part explains the user interface and tools, the second part explains the principles of animation. The third part will showcase creating an animation from start to finish. [Read more about this training course!](/posts/2017/animate-with-krita/)"
    price: "€14.95"
    buyText: "Get Animate with Krita"
    buyUrl: "https://gum.co/TIso?wanted=true"
  comics:
    imageUrl: "images/pages/comics-with-krita.jpg"
    title: "Training: Comics with Krita (Download)"
    description: "By popular demand. the first training DVD is again available. You could also torrent the DVD or watch it on Youtube, but by buying it, you support Krita development! Timothée Giet shows you how to use Krita and Scribus to create comics with only free software."
    price: "€4.95"
    buyText: "Get Comics with Krita"
    buyUrl: "https://gum.co/glye?wanted=true"
  made_2016:
    imageUrl: "images/pages/cover_small.png"
    title: "Made with Krita 2016 (E-edition)"
    description: "You can now also get Made with Krita 2016 as a download! You will get three pdf files. A separate file for the cover and contents, suitable for high-quality printing, and a lower-resolution file, suitable for reading on a tablet or computer."
    price: "€7.95"
    buyText: "Get Made with Krita 2016"
    buyUrl: "https://gum.co/ZVvh"
---

Check out all of the Krita merchandise and training material! All proceeds go to support the development of Krita. We sell through [Gumroad](https://gumroad.com/krita#) because that's currently the best way to handle VAT in Europe. This unfortunately means that the prices listed here are excluding VAT, since the VAT is different in every country.

See the shipping questions at the bottom of the page for more information.

---

{{< shop-item "usbcard" >}}

---

{{< shop-item "bundle" >}}

---

{{< shop-item "bundle2" >}}

---

{{< shop-item "secret" >}}

---

{{< shop-item "muses" >}}

---

{{< shop-item "secret_dl" >}}

---

{{< shop-item "animate" >}}

---

{{< shop-item "comics" >}}

---

{{< shop-item "made_2016" >}}

---

# Shipping

We are located in the Netherlands, Europe. **Physical orders**: we use standard postal delivery. Depending on your location this can take between a couple of days and several weeks.

- **When does shipping happen?** If we receive your order before 16:00 CET, we will send it out the same day, otherwise it'll be the very next day.
- **What days of the week do you process orders?** Monday through to Saturday, except for holidays and the occasional vacation.
- **Where do you ship to?** Everywhere. We ship with ordinary post, not a courier, so there's no tracking information. If you need tracking information, mail foundation@krita.org and we can figure something out. Some countries have a more reliable postal system than others.
