---
title: "Krita 5.1 Release Notes"
date: "2022-04-20"
---

The first release after the big 5.0, Krita 5.1 comes with a ton of smaller improvements and technical polish. This release sees updates to usability across the board, improved file format handling, and a whole lot of changes to the selection and fill tools.

Much thanks for David Revoy for the majority of the feature demonstrations below, as well as Wojtryb for compiling his favorite new features into a video:

{{< youtube TnvCjziCUGI >}}

![](images/2021-11-16_kiki-piggy-bank_krita5.png) 

As a free and open source project, Krita needs your help! Please consider supporting the project by [becoming a member of the development fund](https://fund.krita.org). With your contribution, we can keep a core team of professional developers working on Krita.

### Layers

\[video width="680" height="680" mp4="https://krita.org/wp-content/uploads/2022/07/2022-06-29\_layer-indentation.mp4"\]\[/video\]

- The biggest change to layer handling in Krita 5.1 is Santhosh Anguluri’s Google Summer of Code project 2021: Operations for multiple layers ([MR 888](https://invent.kde.org/graphics/krita/-/merge_requests/888)). This enables copy, cut and paste and clear on when having a selection on multiple layers at once.
- Beyond that there’s a small number of UI tweaks made to the layer docker: You can now control how strongly layers indent when in a group. Furthermore a button got added for showing the context menu when rightclick is unavailable, and finally, file layers now report if their file cannot be found ([MR 1443](https://invent.kde.org/graphics/krita/-/merge_requests/1443), [MR 1213](https://invent.kde.org/graphics/krita/-/merge_requests/1213), [MR 1419](https://invent.kde.org/graphics/krita/-/merge_requests/1419)).
- You can now paint on selection masks with blending modes ([MR 1437](https://invent.kde.org/graphics/krita/-/merge_requests/1437)).

### File Formats

- WebP support ([MR 891](https://invent.kde.org/graphics/krita/-/merge_requests/891) [MR 1268](https://invent.kde.org/graphics/krita/-/merge_requests/1268)). We had basic WebP support already, but this adds full support using [WebP Codec](https://chromium.googlesource.com/webm/libwebp) with every possible toggle imaginable.
- Support for Photoshop layered Tiffs ([MR 944](https://invent.kde.org/graphics/krita/-/merge_requests/944)). Despite Adobe being the steward of the Tiff spec, Photoshop has a unique non-spec compliant way of saving layers into a tiff file: Putting a whole PSD document into the metadata. We now support loading data from such files. On top of this, the export dialog for TIFF was reworked, and we now load and save metadata to tiff ([MR 1015](https://invent.kde.org/graphics/krita/-/merge_requests/1015)).
- OpenExr support for 2.3 and 3+ ([MR 1049](https://invent.kde.org/graphics/krita/-/merge_requests/1049)). Because of a mistake with our build system, Krita couldn’t see OpenExr 2.3 and above, this is fixed now.
- Improve retrieval of image data from clipboard ([MR 1296](https://invent.kde.org/graphics/krita/-/merge_requests/1296), [MR 1431](https://invent.kde.org/graphics/krita/-/merge_requests/1431)). When you copy data between programs, that data is put into the system clipboard. Sometimes when this is done with images programs add several different options. We’ve improved the UI that allows you to choose which of these options you want to use.
- PSD fill layers and color labels ([MR 1309](https://invent.kde.org/graphics/krita/-/merge_requests/1309)). This allows loading and saving layer color labels to PSD, as well as loading and saving the gradient, pattern and color fill layers. This extends the ASL parser inside Krita, which means that layer styles have better color and pattern import as well. Most PSDs use these fill layers in combination with vector masks, which are not yet implemented.
- Support for JPEG-XL ([MR 1363](https://invent.kde.org/graphics/krita/-/merge_requests/1363)). Joining the implementation of Avif last release, JPEG-XL is the newest update of the JPEG image file format, and one of the new set of file formats that is designed to bring wide color gamuts and HDR to the web. We haven’t yet enabled saving and loading of the HDR color spaces to JPEG-XL, but what we do have is saving and loading animations, making JPEG-XL the first fileformat that doesn’t require FFMPEG to allow exporting animations.
- Support for ASE and ACB color palettes ([Commit 6c7ed052](https://invent.kde.org/graphics/krita/-/commit/6c7ed0521fee8736f5dc2f8cd8e4ec5d40ca6070)). The former is used among Adobe applications, the latter is the format used by Photoshop to describe it’s spot-color list. We urge people to be a little careful with the latter as it seems that the Pantone values in particular get updated quite frequently. Discuss with your printer which spot colors you want to use.

### Technical

Some of the biggest changes this time around have been purely technical.

- Fix OpenGl ES for OCIO pipeline ([MR 1262](https://invent.kde.org/graphics/krita/-/merge_requests/1262)), this means OCIO now works on Android. We also use the new GPU pipeline for VFX Platform CY2021 now ([Commit 3d24ed13](https://invent.kde.org/graphics/krita/-/commit/3d24ed14471371dc42c0b0d93e3c0037942412)).
- Build a more recent version of Angle for windows ([MR 1373](https://invent.kde.org/graphics/krita/-/merge_requests/1373), [MR 1353](https://invent.kde.org/graphics/krita/-/merge_requests/1353)). The one we were using was a bit old and this new one has many fixes. Angle has been the most reliable way to allow us to use OpenGL on Windows.
- Added YCbCr profiles ([MR 1330](https://invent.kde.org/graphics/krita/-/merge_requests/1330)). We never shipped YCbCr profiles because there were no open versions of them. This made testing certain features quite hard, in particular file testing. So we [created](https://github.com/amyspark/ycbcr-icc-profiles) some YCbCr profiles and added them as default profiles.
- Add support for llvm-mingw toolchain on windows ([MR 1372](https://invent.kde.org/graphics/krita/-/merge_requests/1372)).
- Allow compiling with RISC-V ([MR 1416](https://invent.kde.org/graphics/krita/-/merge_requests/1416)). None of us own RISC-V hardware, so this one’s mostly for enthusiasts.
- Port krita from VC to XSIMD ([MR 1404](https://invent.kde.org/graphics/krita/-/merge_requests/1404)). We use these libraries for so-called ‘vector instructions’, which is a special way of using the computer that works especially well for repetitive math like blending and mixing colors together. VC’s development has stalled, so switching to XSIMD now allows us to worry less about the future. A nice side effect is that colored and lightness brush tips ended up with a good speed boost, as well as having a speed boost on ARM devices (that’s tablets and mobile phones).
- Add an option to build Krita with precompiled headers ([MR 1471](https://invent.kde.org/graphics/krita/-/merge_requests/1471)). This is something that’s mostly useful for developers, as it’ll reduce compilation.
- Give better information about the location of the AppData directory on windows ([MR 1304](https://invent.kde.org/graphics/krita/-/merge_requests/1304)). The AppData directory can differ greatly when using Krita from the Windows Store, so we wanted to inform the folks that use that method better.

### Usability

- The specific color selector has been extended with a color preview for and a HSV option for RGB. ([MR 916](https://invent.kde.org/graphics/krita/-/merge_requests/916), [MR 1434](https://invent.kde.org/graphics/krita/-/merge_requests/1434)).
- Touch gestures (dragging fingers to pan, zoom, rotate) are now configurable, you can choose which action does what ([MR 1341](https://invent.kde.org/graphics/krita/-/merge_requests/1341)).
- We used to have a very vague button that said something like ‘use aspect of pixels’, right next to the zoom. What this mysterious button actually did was switch the canvas zoom between showing pixels at the size of pixels, and showing the canvas at it’s physical size. The latter, if Krita has the available information, means that an inch in Krita is an actual inch on your monitor, which is useful for print, while the former is more useful for digital-only work like game graphics. This button has been revamped to communicate this more clearly ([MR 1220](https://invent.kde.org/graphics/krita/-/merge_requests/1220)).
- More configuration for the pop-up palette ([MR 922](https://invent.kde.org/graphics/krita/-/merge_requests/922)).
- Dual color selector shortcut ([MR 1100](https://invent.kde.org/graphics/krita/-/merge_requests/1100))
- Missing filter shortcuts ([MR 1109](https://invent.kde.org/graphics/krita/-/merge_requests/1109))
- Add ability to switch from scrolling selecting predefined zoom levels to ‘smooth zoom’ ([MR 1138](https://invent.kde.org/graphics/krita/-/merge_requests/1138)).
- The recent files menu has been overhauled ([MR 1258](https://invent.kde.org/graphics/krita/-/merge_requests/1258), [MR 1283](https://invent.kde.org/graphics/krita/-/merge_requests/1283), [MR 1295](https://invent.kde.org/graphics/krita/-/merge_requests/1295), [MR 1297](https://invent.kde.org/graphics/krita/-/merge_requests/1297), [MR 1307](https://invent.kde.org/graphics/krita/-/merge_requests/1307)).
- Right to left layout improvements ([MR 1267](https://invent.kde.org/graphics/krita/-/merge_requests/1267))
- Assorted fixes various dialogs ([MR 1317](https://invent.kde.org/graphics/krita/-/merge_requests/1317), [MR 1328](https://invent.kde.org/graphics/krita/-/merge_requests/1328), [MR 1331](https://invent.kde.org/graphics/krita/-/merge_requests/1331))
- hsv sliders to the adjustment filters. ([MR 1438](https://invent.kde.org/graphics/krita/-/merge_requests/1438)).
- Only restrict pivot to transform bound when alt is pressed ([MR 1440](https://invent.kde.org/graphics/krita/-/merge_requests/1440)).
- Reset button and saving state for the Digital Color Mixer ([MR 1361](https://invent.kde.org/graphics/krita/-/merge_requests/1361)).
- New Zoom-to-fit(with margins) option ([MR 1344](https://invent.kde.org/graphics/krita/-/merge_requests/1344)).

\[video width="680" height="680" mp4="https://krita.org/wp-content/uploads/2022/07/2022-06-30\_hsv-sliders.mp4"\]\[/video\]

### Fill and Selection tools

Deif Lou has spend the last 6 months greatly improving the functionality of the fill and contiguous selection tools.

These tools now have better organized tool options ([MR 1360](https://invent.kde.org/graphics/krita/-/merge_requests/1360), [MR 1453](https://invent.kde.org/graphics/krita/-/merge_requests/1453)), which is necessary, because there’s now sliders for variable softness ([MR 1174](https://invent.kde.org/graphics/krita/-/merge_requests/1174)) and proper anti-aliasing based on the FXAA algorithm ([MR 1350](https://invent.kde.org/graphics/krita/-/merge_requests/1350)).

There’s also three new ways of applying a fill. The first is _Continuous Fill_ ([MR 1160](https://invent.kde.org/graphics/krita/-/merge_requests/1160)), which allows dragging the cursor over all regions you wish to fill.

\[video width="800" height="600" mp4="https://krita.org/wp-content/uploads/2022/07/2022-07-11\_enclose-and-fill-tool.mp4"\]\[/video\]

The second is a whole new _Enclose and fill_ tool ([MR 1415](https://invent.kde.org/graphics/krita/-/merge_requests/1415)), which allows dragging out a rectangle or other shape over everything you wish to fill, and Krita will automatically determine which sections to fill.

The final one consist of massive improvements to the drag-and-drop swatch functionality ([MR 1135](https://invent.kde.org/graphics/krita/-/merge_requests/1135)). You can now drop swatches onto a section of the canvas, and Krita will use the current fill tool options to determine how the dropped color will fill the section.

### Brushes

\[video width="680" height="680" mp4="https://krita.org/wp-content/uploads/2022/07/2022-06-28-\_eraser-cursor.mp4"\]\[/video\]

- Gui option to configure max brush speed ([MR 996](https://invent.kde.org/graphics/krita/-/merge_requests/996)).
- Added shortcut setting for flow, fade and scatter ([MR 1037](https://invent.kde.org/graphics/krita/-/merge_requests/1037)).
- Make erasers snapping to assistant optional ([MR 1199](https://invent.kde.org/graphics/krita/-/merge_requests/1199)).
- Add more particle distributions to the spray engine ([MR 1377](https://invent.kde.org/graphics/krita/-/merge_requests/1377)).
- Make sharpness options outline alignment behaviour optional ([MR 1405](https://invent.kde.org/graphics/krita/-/merge_requests/1405)). Depending on how you do pixel art, you may or may not want this.
- Allow separate cursor settings for erasers ([MR 1426](https://invent.kde.org/graphics/krita/-/merge_requests/1426)).
- Add shortcut setting to toggle brush outline ([MR 1430](https://invent.kde.org/graphics/krita/-/merge_requests/1430)).
- Sketch Brush Engine now has anti-aliasing available ([MR 1425](https://invent.kde.org/graphics/krita/-/merge_requests/1425)).

### Other

- Option to hold last frame in recorder docker export, ([MR 1087](https://invent.kde.org/graphics/krita/-/merge_requests/1087)).
- Add option to show result at beginning for recorder export ([MR 1293](https://invent.kde.org/graphics/krita/-/merge_requests/1293)).
- Added a perspective ellipse assistant tool ([MR 1343](https://invent.kde.org/graphics/krita/-/merge_requests/1343)). Srirupa Datta’s 2022 KDE Season of Code project, the perspective ellipse tool assistant helps drawing circles in perspective.
- Subdivisions for ruler assistants ([MR 1298](https://invent.kde.org/graphics/krita/-/merge_requests/1298)).
- Improvements to the screentone generator ([MR 1010](https://invent.kde.org/graphics/krita/-/merge_requests/1010)), the results of the generator can now be made more consistent.
- Various fixes for G’Mic ([MR 1464](https://invent.kde.org/graphics/krita/-/merge_requests/1464)).
- We now warn when the save operation failed ([MR 1410](https://invent.kde.org/graphics/krita/-/merge_requests/1410)).
- The Levels filter can now be applied per-channel ([MR 1067](https://invent.kde.org/graphics/krita/-/merge_requests/1067)).

{{< support-krita-callout >}}