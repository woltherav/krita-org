---
title: "Krita 5.1.4 Released"
date: "2022-12-14"
categories: 
  - "news"
  - "officialrelease"
---

We're releasing today a new bugfix release. This probably will be the last 5.1 bugfix release, since we're updating our dependencies and builds after this. Next will be 5.2 with a ton of changes!

## Fixes

- Vector shapes not swapping the current fg/bg color ([BUG:461692](https://bugs.kde.org/show_bug.cgi?id=461692))
- Fix a crash when using "Paste into Active Layer" ([BUG:462223](https://bugs.kde.org/show_bug.cgi?id=462223))
- Layer Styles: label the Outer Glow page as Outer, not Inner Glow. ([BUG:462091](https://bugs.kde.org/show_bug.cgi?id=462091))
- Parse transfer characteristics from ICC profiles. Patch by Rasyuqa, thanks! (BUG:45911)
- Fix handling ICC color primaries and white point detections. Patch by Rasyuqa, thanks!
- Remove two obsolete actions from the action list in settings->configure Krita->shortcuts
- Fix some display artifacts when using fractional display scaling. (BUG:441216, 460577, 461912)
- Fix wraparound mode for non-pixel brush engines (BUG:460299)
- Fix visibility of the measure and gradient tools on a dark background
- Fix data loss when a transform tool is applied too quickly. (BUG:460557, 461109)
- Android: Disable changing the resource location
- Android: Disable the touch docker (some buttons are completely broken, and we're rewriting Krita's touch functionality). BUG:461634
- Android: disable New Window (Android does not do windows).
- Android: disable workspaces that create multiple windows (Android does not do windows).
- Android: make TIFF import and export work
- Android: remove the detach canvas action (Android does not do windows).
- TIFF: Fix inconsistent alpha and Photoshop-style layered tiff export checkboxes (BUG:462925)
- TIFF: Fix handling multipage files (BUG:461975)
- TIFF: Implement detection of the resolution's unit. (BUG:420932)
- EXR: Implement consistent GRAY and XYZ exporting (BUG:462799)
- AVIF: add the image/avif mimetype to the desktop file so external applications can know Krita can open these files. (BUG:462224)
- PSD: allow zero-sized resource blocks (BUG:461493, 450983)
- Python: fix creating a new image from Python. (BUG:462665)
- Python: fix updating the filename when using Document::saveAs.  (BUG:462667)
- Python: make it possible to use Python 3.11 (BUG:461598)
- Animation: Improve the autokey functionality. (BUG:459723)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.4-setup.exe](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.4.zip](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.4-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.4.dmg](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-x86_64-5.1.4-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-x86-5.1.4-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-arm64-v8a-5.1.4-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-armeabi-v7a-5.1.4-release-signed.apk)

### Source code

- [krita-5.1.4.tar.gz](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.gz)
- [krita-5.1.4.tar.xz](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.1.4/](https://download.kde.org/stable/krita/5.1.4) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.1.4/) (filenames ending in .sig).
