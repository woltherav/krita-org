---
title: "Intel Becomes First Krita Development Fund Corporate Gold Patron"
date: "2022-09-19"
---

The Krita Foundation is very happy to announce that Intel is the first Patron Member of the Foundation's Development Fund Intel and Krita have a long history together of successful collaboration on projects like Krita Sketch, improving multithreading and HDR painting.

> This strategic collaboration will deliver a series of new painting capabilities, making full use of the 12th and future Gen’s Intel Core Hybrid technology using powerful P-cores and E-cores, as well as the Intel Arc GPUs, all resulting into a more powerful painting experience with less lag. We are also excited about the support for JPEG XL, offering significantly better HDR experience and higher compression ratios, hereby meeting the needs of image delivery on the web and professional photography.’ Jerry Tsao, VP/GM of Intel CCG Mobile Enthusiast & Creator Segment.

With Intel’s support, Krita can build a more stable developer foundation, allowing to hire and retain the best developers in the industry.

With this new Intel partnership, we are already seeing improvements. A recent result is in the Krita 5.1 release: full support for the new JPEG-XL file format. JPEG-XL is a new image format that offers significantly better compression and image quality than normal JPG images. The file format is currently behind experimental flags in most web browsers, so it is an upcoming format that we want to support. Together with Intel we are working with the JPEG-XL developers and the Chrome developers to ensure interoperability.

In the future, the Krita community will collaborate with Intel in the creation of technical documentation in the form of white papers. These white papers will explore new art and painting technologies. Krita will add the new ideas and features in the application, and assist Intel in writing the white paper explaining the new technologies.

### What is the Krita Development Fund?

The Krita Development Fund accepts donations to support sponsored developers to work on exciting new features, performance improvements and stability improvements, as well as outreach to users in the form of manuals, tutorials and resources for painters.

Learn more at [fund.krita.org](https://fund.krita.org)


![](images/posts/2022/Intel_logo-classicblue-3000px-300x122.png)

![](images/posts/2022/krita_logo_extra-300x137.png)
